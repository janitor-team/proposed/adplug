adplug (2.3.3+dfsg-2) unstable; urgency=medium

  * Upload to unstable

 -- Yangfl <mmyangfl@gmail.com>  Wed, 15 Jul 2020 15:39:35 +0800

adplug (2.3.3+dfsg-1) experimental; urgency=medium

  * New upstream release
    - Fix CVE-2019-14692 (Closes: #943927)
    - Fix CVE-2019-14691 (Closes: #943928)
    - Fix CVE-2019-14690 (Closes: #943929)
    - Fix CVE-2019-15151 (Closes: #946340)
  * Bump debhelper compat to 13

 -- Yangfl <mmyangfl@gmail.com>  Sat, 13 Jun 2020 11:40:08 +0800

adplug (2.3.2+dfsg-1) experimental; urgency=medium

  * New upstream release (Closes: #961480)
    - Fix CVE-2019-14692 (Closes: #943927)
    - Fix CVE-2019-14691 (Closes: #943928)
    - Fix CVE-2019-14690 (Closes: #943929)
    - Fix CVE-2019-15151 (Closes: #946340)
  * Bump Standards-Version to 4.5.0
  * Add upstream metadata

 -- Yangfl <mmyangfl@gmail.com>  Fri, 29 May 2020 23:11:55 +0800

adplug (2.3.1+dfsg-2) unstable; urgency=medium

  * Upload to unstable
  * Bump Standards-Version to 4.4.1

 -- Yangfl <mmyangfl@gmail.com>  Mon, 14 Oct 2019 17:50:54 +0800

adplug (2.3.1+dfsg-1) experimental; urgency=medium

  * New Maintainer (Closes: #918955)
  * New upstream release
    - Fix FTBFS with GCC-9 (Closes: #925627)
    - Move adplug.pc to a multiarch location (Closes: #930204)
  * Bump Standards-Version to 4.4.0
  * Bump debhelper compat to 12

 -- Yangfl <mmyangfl@gmail.com>  Fri, 11 Oct 2019 21:35:27 +0800

adplug (2.2.1+dfsg3-0.4) unstable; urgency=medium

  * Non-maintainer upload.
  * Add 06-fix-chnresolv-signage.diff to fix signage
    of Cs3mPlayer::chnresolv array. (Closes: #8570222)

 -- John Paul Adrian Glaubitz <glaubitz@physik.fu-berlin.de>  Tue, 07 Mar 2017 11:50:44 +0100

adplug (2.2.1+dfsg3-0.3) unstable; urgency=medium

  * Non-maintainer upload, applying changes from Ubuntu.
  * After binNMUs for the affected packages, the archive will
    no longer contain a libadplug-2.2.1-0 with an ABI incompatible
    with the one in testing (Closes: #795014)

  [ Iain Lane ]
  * Rename for gcc5 transition
  * Artificially version libbinio BD so that we wait until the transitioned
    version is available - can be dropped later.

 -- Simon McVittie <smcv@debian.org>  Fri, 21 Aug 2015 20:44:20 +0100

adplug (2.2.1+dfsg3-0.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Use dh-autoreconf so package builds on new architectures 
     (Closes: #757896, #785117, #759440, #765193)

 -- Wookey <wookey@debian.org>  Sat, 23 May 2015 00:04:25 +0100

adplug (2.2.1+dfsg3-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * Create a 2.2.1+dfsg3 tarball with
    doc/{fdl.texi,libadplug.texi,libadplug.info} removed.
    Additionally revert doc/Makefile.in to its pristine state from 2.2.1+dfsg1
    and "flip" debian/patches/04-no-gfdl-docs.diff.
    Closes: #695710

 -- gregor herrmann <gregoa@debian.org>  Fri, 25 Jan 2013 16:36:59 +0100

adplug (2.2.1+dfsg2-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Repack upstream tarball to exclude GFDL documentation 
    (doc/{fdl.texi,libadplug.texi,libadplug.info} with
    unmodifiable sections. Closes: #695710
   + Add 04-no-gfdl-docs.diff to skip building of GFDL docs.

 -- Jelmer Vernooij <jelmer@debian.org>  Sun, 16 Dec 2012 14:08:10 +0100

adplug (2.2.1+dfsg1-1) unstable; urgency=low

  * New maintainer (closes: #454268).
  * Switch to dpkg-source 3.0 (quilt) format.
  * Dropped patches (no longer required):
    + debian/01-include.dpatch
    + debian/02-fix-ftbfs-with-gcc-4.3.dpatch
  * Tagged 03-no-tests.diff with a DEP-3 header.
  * debian/control:
    - add libbinio-dev dependency to libadplug-dev (closes: #510276).
    - reference OPL3 in package description (closes: #479786).
  * debian/copyright: updated to the latest DEP-5 revision.
  * debian/rules: using dh7 style rules.
  * debian/watch: created.
  * Shared library package renamed to libadplug-2.2.1-0.
  * Unnecessary .la file removed (closes: #621144).
  * Bump Standards-Version to 3.9.2.

 -- Артём Попов <artfwo@ubuntu.com>  Wed, 25 May 2011 22:23:53 +0700

adplug (2.0.1.dfsg-1) unstable; urgency=low

  * QA upload.
  * Repack upstream tarball to not include test/ directory as it contains
    non-DFSG compliant samples from proprietary games. (Closes: #532974)
  * Bump Standards-Version to 3.8.2.

 -- Chris Lamb <lamby@debian.org>  Mon, 27 Jul 2009 10:40:47 +0200

adplug (2.0.1-7) unstable; urgency=low

  * QA upload.
  * Fix FTBFS with gcc-4.3 by fixing missing includes (Closes: #417078).
     + debian/patches/02-fix-ftbfs-with-gcc-4.3.dpatch
  * No longer ignore “make distclean” errors.
  * Move the Homepage from the descriptions to a source field.
  * Bump Standards-Version (no changes needed).

 -- Cyril Brulebois <cyril.brulebois@enst-bretagne.fr>  Mon, 17 Mar 2008 02:24:23 +0100

adplug (2.0.1-6) unstable; urgency=low

  * Orphaning package, setting maintainer to the Debian QA Group.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Wed, 05 Dec 2007 16:12:15 +0100

adplug (2.0.1-5) unstable; urgency=low

  * New Maintainer. (Closes: #407626)

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Mon, 16 Apr 2007 12:31:08 +0200

adplug (2.0.1-4) unstable; urgency=low

  * Debian QA Group <packages@qa.debian.org>.

 -- Daniel Baumann <daniel@debian.org>  Sat, 20 Jan 2007 09:57:00 +0100

adplug (2.0.1-3) unstable; urgency=low

  * Minor cleanups.

 -- Daniel Baumann <daniel@debian.org>  Fri, 19 Jan 2007 10:14:00 +0100

adplug (2.0.1-2) unstable; urgency=low

  * The previous upload did not take care about the bumped soname, therefore
    updated the shlibs now. This requires all packages build-depending on adplug
    to be recompiled against 2.0.1-2. Fortunately, this affects only ocp and
    adplay*.

 -- Daniel Baumann <daniel@debian.org>  Sun, 17 Sep 2006 18:37:00 +0200

adplug (2.0.1-1) unstable; urgency=high

  * New upstream release:
    - fixes multiple remote stack-based buffer overflows CVE-2006-3582
      CVE-2006-3581 (Closes: #378279).

 -- Daniel Baumann <daniel@debian.org>  Mon, 17 Jul 2006 19:48:00 +0200

adplug (2.0-3) unstable; urgency=low

  * New email address.

 -- Daniel Baumann <daniel@debian.org>  Thu,  6 Jul 2006 07:17:00 +0200

adplug (2.0-2) unstable; urgency=low

  * Fixed build-depends (Closes: #375713).

 -- Daniel Baumann <daniel.baumann@panthera-systems.net>  Wed, 28 Jun 2006 08:36:00 +0200

adplug (2.0-1) unstable; urgency=low

  * New upstream release.

 -- Daniel Baumann <daniel.baumann@panthera-systems.net>  Sun, 25 Jun 2006 12:43:00 +0200

adplug (1.5.1-6) unstable; urgency=low

  * Rebuild and rename due to new libstdc++ configuration (Closes: #339143).

 -- Daniel Baumann <daniel.baumann@panthera-systems.net>  Sun, 20 Nov 2005 17:01:00 +0200

adplug (1.5.1-5) unstable; urgency=low

  * Added patch to update includes of new libbinio1c2 (Closes: #335825).

 -- Daniel Baumann <daniel.baumann@panthera-systems.net>  Sat, 29 Aug 2005 16:49:00 +0200

adplug (1.5.1-4) unstable; urgency=low

  * Recompiled against fixed libbinio1c2 to correct depends (Closes: #321573).
  * Added override for libadplug0c2 complaining about package name.
  * Removed unnecessary provides-fields.

 -- Daniel Baumann <daniel.baumann@panthera-systems.net>  Sat,  6 Aug 2005 13:34:00 +0200

adplug (1.5.1-3) unstable; urgency=low

  * C++ ABI change.
  * bumped policy to 3.6.2.

 -- Daniel Baumann <daniel.baumann@panthera-systems.net>  Fri,  8 Jul 2005 15:18:00 +0200

adplug (1.5.1-2) unstable; urgency=low

  * debian/rules: reverted binary-targets to default (Closes: #312025).

 -- Daniel Baumann <daniel.baumann@panthera-systems.net>  Sun,  5 Jun 2005 16:52:00 +0100

adplug (1.5.1-1) unstable; urgency=low

  * Initial Release.

 -- Daniel Baumann <daniel.baumann@panthera-systems.net>  Wed, 11 May 2005 12:38:00 +0200
